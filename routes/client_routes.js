const express = require('express');
const router = express.Router();
const { isClient } = require('../middleware/aut_jwt');
const { registration, login, showAccount, editAccount, getAllDishesByCategory,showReviewByTitle,showReview, Login,test,test2,LoginTOKEN } = require('../controllers/client_functions');
const { reservationDish, showAllReservations, editReservation, validateReservation, deleteDishesfromReservation, showMyReservations, deleteReservation } = require('../controllers/reservation_function');
const { occupyTable, showMyTables, deleteOccupation, showFreeTables } = require('../controllers/occupy_function');
const { writeReview, showMyReviews, deleteReview } = require('../controllers/review_functions');
const { validationResult, body } = require('express-validator');
var client;
const { createDish, showAllDishes, editDish, dishesByCategory, showDetailDish, deleteDish } = require('../controllers/dish_function')

/** MODULO CLIENT ROUTES -> RACCHIUDE TUTTE LE ROTTE ALLE VARIE FUNZIONI INVOCABILI DALL'ENTITA' CLIENT**/
// API CLIENT DOC: https://documenter.getpostman.com/view/11778714/Szzn4vAE?version=latest#9463f0ba-05d4-49ac-b4b9-a1c6bf498a4b


router.post('/login', [
    // controllo dati prima di prcedere al login
    body('password').trim().notEmpty().withMessage('Password < 5 Caratteri'),
    body('email').trim().notEmpty().isEmail().withMessage('email is not valid')

], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array(),
        });
    } else {
        const { email, password } = req.body;
        login(req, res, { email, password });
    }
});



router.post('/signup', [

    // controllo dati prima di prcedere alla registrazione
    body('password').trim().notEmpty().isLength({ min: 5 }).withMessage('Password < 5 Caratteri'),
    body('email').trim().notEmpty().isEmail().withMessage('email is not valid'),
    body('role').trim().notEmpty().withMessage('role is not valid'),
    body('username').trim().notEmpty().withMessage('username is not valid'),


], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array(),
        });
    } else {

        const { email, username, password, role, confirmpassword } = req.body;

        if (password !== confirmpassword) {
            return res.status(400).json({
                message: 'Campi password e confirm password diversi',
            });
        } else {
            registration(req, res, { email, username, password, role });
        }
    }
});



router.get('/show_account', function(req, res) {
    client = isClient(req, res);
    if (client) {
        showAccount(req, res, client)
    }
});



router.post('/edit_account', function(req, res) {
    client = isClient(req, res);
    if (client) {
        editAccount(req, res, client)
    }
});



router.post('/dish_reservation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        reservationDish(req, res, client)
    }
});



router.get('/show_all_reservation', function(req, res) {

    client = isClient(req, res);
    if (client) {
        showAllReservations(req, res, client)
    }

});


router.post('/occupy_table', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        occupyTable(req, res, client)
    }
});



router.get('/show_tables', function(req, res) {
    client = isClient(req, res);
    if (client) {
        showMyTables(req, res, client)
    }
});


router.get('/show_free_tables', function(req, res) {
    client = isClient(req, res);
    if (client) {
        showFreeTables(req, res, client)
    }
});


router.get('/detail_dish', function(req, res) {
    client = isClient(req, res);
    if (client) {
        showDetailDish(req, res)
    }
});


router.get('/category_dish', function(req, res) {
    client = isClient(req, res);
    if (client) {
        dishesByCategory(req, res)
    }
});


router.post('/delete_occupation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        deleteOccupation(req, res, client)
    }
});


router.post('/edit_reservation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        editReservation(req, res, client)
    }
});


router.post('/delete_dishes_reservation', validateReservation, function(req, res) {
    client = isClient(req, res);
    if (client) {
        deleteDishesfromReservation(req, res, client)
    }
});


router.get('/my_reservations', function(req, res) {
    client = isClient(req, res);
    if (client) {
        showMyReservations(req, res, client)
    }
});


router.post('/write_review', function(req, res) {
    client = isClient(req, res);
    if (client) {
        writeReview(req, res, client)
    }
});


router.get('/show_reviews', function(req, res) {
    client = isClient(req, res);
    if (client) {
        showMyReviews(req, res, client)
    }
});


router.post('/delete_review', function(req, res) {
    client = isClient(req, res);
    if (client) {
        deleteReview(req, res, client)
    }
});


router.get('/all_dishes', function(req, res) {
    client = isClient(req, res);
    if (client) {
        getAllDishesByCategory(req, res)
    }
});


router.post('/delete_all_reservation', function(req, res) {
    client = isClient(req, res);
    if (client) {
        deleteReservation(req, res, client)
    }
});



/**********************QUERY ATTACCABILI*********************/

router.get('/show_review_by_title', function(req, res) {
        showReviewByTitle(req, res, client)
});

router.get('/show_rev', function(req, res) {
        showReview(req, res)
});


router.post('/Login1', function(req, res) {
    const {email,password}=req.body
        Login(req, res,{email,password});
});

router.post('/test', function(req, res) {
   
        test(req, res);
});

router.post('/test2', function(req, res) {
   
    test2(req, res);
});



/**********************QUERY ATTACCABILI TOKEN UPDATE*********************/

router.get('/show_review_by_titleTOKEN', function(req, res) {
    client = isClient(req, res);
    if (client) {
    showReviewByTitle(req, res, client)
    }
});

router.get('/show_revTOKEN', function(req, res) {
    client = isClient(req, res);
    if (client) {
    showReview(req, res)
    }
});


router.post('/Login1TOKEN', function(req, res) {
const {email,password}=req.body
    LoginTOKEN(req, res,{email,password});
});

router.post('/testTOKEN', function(req, res) {
    client = isClient(req, res);
    if (client) {
    test(req, res);
    }
});

router.post('/test2TOKEN', function(req, res) {
    client = isClient(req, res);
    if (client) {
     test2(req, res);
    }
});

module.exports = router;