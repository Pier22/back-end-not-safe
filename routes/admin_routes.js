const express = require('express');
const router = express.Router();
const { isAdmin } = require('../middleware/aut_jwt');
const { deleteReview } = require('../controllers/review_functions');
const { showAllClients, deleteClient, editClient, showClient, newClient, showReview, newStaff, editStaff, deleteStaff, showStaff, showAllStaff } = require('../controllers/admin_function')
const { createDish, showAllDishes, editDish, deleteDish } = require('../controllers/dish_function');
const { login,findItems,Login,LoginTOKEN,showClientAtt} = require('../controllers/admin_function');
const { getAllIngredients, newIngredient, deleteIngredient, renameIngredient } = require('../controllers/ingredient_function');
const { getAllAllergens, newAllergen, deleteAllergen, renameAllergen } = require('../controllers/allergen_function')
const { validationResult, body } = require('express-validator');
const { newRole } = require('../controllers/role_function');
const {apiLimiter} = require('../middleware/Limiter')


/** MODULO ADMIN ROUTES -> RACCHIUDE TUTTE LE ROTTE ALLE VARIE FUNZIONI INVOCABILI DALL'ENTITA' ADMIN**/
// API AMIN DOC : https://documenter.getpostman.com/view/11778714/Szzn4vAE?version=latest#be5f9427-62e1-4fa9-bc50-ca91b6d3949b


router.post('/login',apiLimiter, [
    body('username').trim().notEmpty().withMessage('username is not valid'),
    body('password').trim().notEmpty().withMessage('Password < 5 Caratteri')

], (req, res) =>  {
   
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    } else {
        const { username, password } = req.body;
        
        login(req, res, { username, password });
    }
});


router.post('/delete_dish', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        deleteDish(req, res)
    }

});


router.post('/new_dish', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        createDish(req, res, admin)
    }

});


router.get('/show_dishes', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        showAllDishes(req, res)
    }
});


router.post('/edit_dish', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        editDish(req, res, admin)
    }

});



router.post('/delete_review', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        deleteReview(req, res, admin)
    }

});



router.get('/show_clients', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        showAllClients(req, res)
    }

});



router.post('/delete_client', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        deleteClient(req, res)
    }
});



router.post('/edit_client', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        editClient(req, res)
    }

});



router.get('/show_client_', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        showClient(req, res)
    }

});



router.post('/new_client', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        newClient(req, res)
    }
});



router.post('/new_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        newStaff(req, res)
    }

});


router.get('/show_all_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        showAllStaff(req, res)
    }

});


router.post('/delete_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        deleteStaff(req, res)
    }

});


router.post('/edit_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        editStaff(req, res)
    }

});



router.get('/show_staff', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        showStaff(req, res)
    }

});



router.get('/show_all_review', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        showReview(req, res)
    }
});


router.get('/show_all_review', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        showReview(req, res)
    }
});




router.get('/get_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        getAllIngredients(req, res)
    }
});




router.get('/get_allergens', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        getAllAllergens(req, res)
    }
});


router.post('/new_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        newIngredient(req, res)
    }
});


router.post('/new_allergen', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        newAllergen(req, res)
    }
});


router.post('/delete_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        deleteIngredient(req, res)
    }
});


router.post('/delete_allergen', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        deleteAllergen(req, res)
    }
});


router.post('/rename_ingredient', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        renameIngredient(req, res)
    }
});


router.post('/rename_allergen', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        renameAllergen(req, res)
    }
});



router.post('/new_role', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
        newRole(req, res)
    }
});

/***************QUERY ATTACCABILI**************** */
router.get('/finddish', function(req, res) {
   
    findItems(req,res)
    
});

router.post('/Login1', function(req, res) {
    const {username,password}=req.body
        Login(req, res,{username,password});
});

router.get('/show_client', function(req, res) {
    
        showClientAtt(req, res);
});


/***************QUERY ATTACCABILI TOKEN UPDATE**************** */
router.get('/finddishTOKEN', function(req, res) {
    admin = isAdmin(req, res)
    if (admin) {
    findItems(req,res)
    }
});

router.post('/Login1TOKEN', function(req, res) {
    const {username,password}=req.body
        LoginTOKEN(req, res,{username,password});
});


module.exports = router;