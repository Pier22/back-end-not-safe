const express = require('express');
const router = express.Router();
const { isStaff, isAdminStaff } = require('../middleware/aut_jwt');
const { editStaffProfile, showTables, showProfile, tableMap, editTable, registration, login, freeTable, categoryDish, createTable } = require('../controllers/staff_function')
const { createDish, showAllDishes, editDish, dishesByCategory, showDetailDish, deleteDish, showDish, getDish, getAllDishes } = require('../controllers/dish_function')
const { validationResult, body } = require('express-validator');
const { getAllCategories, newCategory } = require('../controllers/category_function');
const { getAllIngredients } = require('../controllers/ingredient_function');
const { getAllAllergens } = require('../controllers/allergen_function');
const { getAllRole } = require('../controllers/role_function')
var staff;

/** MODULO STAFF ROUTES -> RACCHIUDE TUTTE LE ROTTE ALLE VARIE FUNZIONI INVOCABILI DALL'ENTITA' STAFF**/
//API STAFF DOC: https://documenter.getpostman.com/view/11778714/Szzn4vAE?version=latest#a59dad2e-afe1-4368-865c-84ec7e883d13

router.post('/signup', [

    // controllo dati prima di prcedere alla registrazione
    body('username').trim().notEmpty().withMessage('username is not valid'),
    body('password').trim().notEmpty().isLength({ min: 5 }).withMessage('Password < 5 Caratteri')

], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    } else {

        const { username, password, confirmpassword } = req.body;

        if (password !== confirmpassword) {
            console.log("passworde sbagliata");
            return res.sendStatus(400);
        } else {
            registration(req, res, { username, password });
        }
    }
});


router.post('/login', [

    body('username').trim().notEmpty().withMessage('username is not valid'),
    body('password').trim().notEmpty().withMessage('Password < 5 Caratteri')

], (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    } else {
        const { username, password } = req.body;

        login(req, res, { username, password });

    }
});


router.get('/category_dish', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        dishesByCategory(req, res)
    }
});


router.post('/delete_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        deleteDish(req, res)
    }
});


router.get('/detail_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        showDetailDish(req, res)
    }
});


router.post('/new_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        createDish(req, res, staff)
    }

});


router.get('/show_dishes', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        showAllDishes(req, res)
    }

});



router.post('/edit_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        editDish(req, res, staff)
    }

});


router.get('/show_tables', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        showTables(req, res)
    }

});



router.get('/show_profile', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        showProfile(req, res, staff)
    }
});


router.post('/edit_profile', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        editStaffProfile(req, res, staff)
    }
});



router.get('/table_map', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        tableMap(req, res)
    }
});


router.post('/edit_table', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        editTable(req, res, staff)
    }
});


router.get('/free_table', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        freeTable(req, res, staff)
    }
});


router.get('/get_categories', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        getAllCategories(req, res)
    }
});


router.get('/get_ingredients', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        getAllIngredients(req, res)
    }
});


router.get('/get_allergens', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        getAllAllergens(req, res)
    }
});


router.get('/get_category', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        categoryDish(req, res)
    }
});


router.get('/get_role', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        getAllRole(req, res)
    }
});


router.get('/show_dish', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        showDish(req, res)
    }
});


router.post('/new_table', function(req, res) {
    staff = isStaff(req, res)
    if (staff) {
        createTable(req, res, staff)
    }
});


router.get('/get_dish_by_uuid', function(req, res) {
    //staff = isAdminStaff(req, res)
    //if (staff) {
        getDish(req, res, staff)
    //}
});


router.get('/get_all_dishes', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        getAllDishes(req, res, staff)
    }
});


router.post('/new_category', function(req, res) {
    staff = isAdminStaff(req, res)
    if (staff) {
        newCategory(req, res, staff)
    }
});

module.exports = router;