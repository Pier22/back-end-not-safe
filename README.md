# UniBookBar back-end
Seconda parte del Progetto di sicurezza, dalla la quale si evincono i difetti di progettazione e le falle di sicurezza dovute ad una cattiva programmazione ed ad un uso non corretto delle buone pratiche di implementazione/progettazione e design.

il fokus di tal progetto è appunto quello di mostrare come un codice mal implementato e privo di una qual si voglia progettazione volta alla sicurezza sin dalla fase di design porti a delle criticità , vulnerabilità ed una scarsa manutenzione del prodotto nel tempo. Inoltre anche l'affidarsi semplicemnete ad un architettura ben organizzata non comporti l'ottimizzazione del prodotto.
I
Back-end repository - UniBookBar Project

## Risorse
[progetto](https://gitlab.com/Pier22/back-end-security)

