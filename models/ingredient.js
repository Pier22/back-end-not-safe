module.exports = (sequelize, Sequelize) => {
    const Ingredient = sequelize.define('ingredients', {
        UuidIngredient: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    return Ingredient;
};



