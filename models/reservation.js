const db = require("../models/connection");

module.exports = (sequelize, Sequelize) => {
    const Reservation = sequelize.define('reservation', {
        UuidReservation: {
            primaryKey:true,
            type: Sequelize.UUID,
            allowNull: false,
        },
        Date: {
            type: Sequelize.DATEONLY,
            //allowNull: false
        },
        Hour: {
            type: Sequelize.TIME,
            //allowNull: false
        },
        Quantity: {
            type: Sequelize.INTEGER,
            //allowNull: false
        }
    }, {freezeTableName: true});

    return Reservation;
};

