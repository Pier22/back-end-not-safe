module.exports = (sequelize, Sequelize) => {
    const Category = sequelize.define('categories', {
        UuidCategory: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        CategoryName: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    return Category;
};



