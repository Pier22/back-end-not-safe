module.exports = (sequelize, Sequelize) => {
    const Writing = sequelize.define('writing', {
        Date: {
            type: Sequelize.DATEONLY,
            //allowNull: false
        },
        Hour: {
            type: Sequelize.TIME,
            //allowNull: false
        },

    }, {freezeTableName: true});

    return Writing;
};