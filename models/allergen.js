module.exports = (sequelize, Sequelize) => {
    const Allergen = sequelize.define('allergens', {
        UuidAllergen: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    return Allergen
};

