module.exports = (sequelize, Sequelize) => {
    const Dish = sequelize.define('dishes', {
        UuidDish: {
            type: Sequelize.UUID,
            primaryKey: true,
            allowNull: false
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        Unit: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        Price: {
            type: Sequelize.FLOAT(4, 2),
            allowNull: false
        },
        BkbleConble: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        UrlImage: {
            type: Sequelize.STRING,
            allowNull: false
        },
    });
    return Dish;
};

