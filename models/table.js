module.exports = (sequelize, Sequelize) => {
    const Table = sequelize.define('tables', {
        UuidTable: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        Occupied: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        Chairs: {
            type: Sequelize.INTEGER,
            defaultValue: 4
        },
        NumberTable: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue:0,
        }

    });
    return Table;
};