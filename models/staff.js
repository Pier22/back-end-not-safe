module.exports = (sequelize, Sequelize) => {
    const Staff = sequelize.define('staff', {
        UuidStaff: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        Name: {
            type: Sequelize.STRING,
            allowNull: true
        },
        Surname: {
            type: Sequelize.STRING,
            allowNull: true
        },
        Password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        Username: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        CanAdmin: {
            type: Sequelize.INTEGER,
            allowNull: true
        },

    }, { freezeTableName: true });

    return Staff;
};