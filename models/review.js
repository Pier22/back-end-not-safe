const db = require("../models/connection");
module.exports = (sequelize, Sequelize) => {
    const Reviews = sequelize.define('reviews', {
        UuidRev: {
            type: Sequelize.UUID,
            primaryKey: true,
            //allowNull: false
        },
        Title: {
            type: Sequelize.STRING,
            //allowNull: false
        },
        Text:{
            type: Sequelize.STRING,
            //allowNull: false
        },
        Star:{
            type: Sequelize.INTEGER,
            //allowNull: false
        }

    }, {freezeTableName: true});

    return Reviews;
};