module.exports = (sequelize, Sequelize) => {

    const role = sequelize.define('role', {
        UuidRole: {
            type: Sequelize.STRING,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        NameRole: {
            type: Sequelize.STRING,
            allowNull: false
        }

    },{freezeTableName:true});
    return role;
};
