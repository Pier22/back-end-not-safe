module.exports = (sequelize, Sequelize) => {
    const Organizes = sequelize.define('organizes', {
        UuidOrganize: {
            primaryKey:true,
            type: Sequelize.UUID,
            allowNull: false,
        },
    },{freezeTableName:true});
    return Organizes;
};