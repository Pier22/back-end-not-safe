module.exports = (sequelize, Sequelize) => {
    const Manage = sequelize.define('manage', {
        UuidManage: {
            primaryKey:true,
            type: Sequelize.UUID,
            allowNull: false,
        },
        Azione: {
            type: Sequelize.STRING,
        },

    },{freezeTableName:true});
    return Manage;
};