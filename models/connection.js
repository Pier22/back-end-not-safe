const config = require("../config");
const Sequelize = require("sequelize");

// Metto la pass in chiaro solo per comodità
// questa va nel gitignore in un conf a parte
const sequelize = new Sequelize(
    config.DB_NAME,
    config.DB_ACCESS,
    "", {
        host: config.DB_HOST,
        dialect: "mysql",
        // TODO DA SETTARE IL LOGGINGG A FALSE
        logging: true,
        define: {
            timestamps: false,
        },
    }
);

const db = {};
db.client = require("./client")(sequelize, Sequelize);
db.table = require("./table")(sequelize, Sequelize);
db.dish = require("./dish")(sequelize, Sequelize);
db.reservation = require("./reservation")(sequelize, Sequelize);
db.occupy = require("./occupy")(sequelize, Sequelize);
db.review = require("./review")(sequelize, Sequelize);
db.writing = require("./writing")(sequelize, Sequelize);
db.staff = require("./staff")(sequelize, Sequelize);
db.references = require("./references")(sequelize, Sequelize);
db.role = require("./role")(sequelize, Sequelize);
db.play = require("./play")(sequelize, Sequelize);
db.ingredient = require("./ingredient.js")(sequelize, Sequelize);
db.made = require("./made.js")(sequelize, Sequelize);
db.category = require("./category")(sequelize, Sequelize);
db.content = require("./content")(sequelize, Sequelize);
db.allergen = require("./allergen")(sequelize, Sequelize);
db.belong = require("./belong")(sequelize, Sequelize);
db.manage = require("./manage")(sequelize, Sequelize);
db.organizes = require("./organizes")(sequelize, Sequelize);

db.play.belongsTo(db.client, { foreignKey: "UuidClient" });
db.play.belongsTo(db.role, { foreignKey: "UuidRole" });
db.play.removeAttribute("id");

db.references.belongsTo(db.dish, { foreignKey: "UuidDish" });
db.references.belongsTo(db.review, { foreignKey: "UuidRev" });
db.references.removeAttribute("id");

db.writing.belongsTo(db.review, { foreignKey: "UuidRev" });
db.writing.belongsTo(db.client, { foreignKey: "UuidClient" });
db.writing.removeAttribute("id");

db.reservation.belongsTo(db.client, { foreignKey: "UuidClient" });
db.reservation.belongsTo(db.dish, { foreignKey: "UuidDish" });

db.occupy.belongsTo(db.client, { foreignKey: "UuidClient" });
db.occupy.belongsTo(db.table, { foreignKey: "UuidTable" });

db.made.belongsTo(db.dish, { foreignKey: "UuidDish" });
db.made.belongsTo(db.ingredient, { foreignKey: "UuidIngredient" });
db.made.removeAttribute("id");

db.content.belongsTo(db.dish, { foreignKey: "UuidDish" });
db.content.belongsTo(db.allergen, { foreignKey: "UuidAllergen" });
db.content.removeAttribute("id");

db.belong.belongsTo(db.dish, { foreignKey: "UuidDish" });
db.belong.belongsTo(db.category, { foreignKey: "UuidCategory" });
db.belong.removeAttribute("id");

db.organizes.belongsTo(db.staff, { foreignKey: "UuidStaff" });
db.organizes.belongsTo(db.table, { foreignKey: "UuidTable" });

db.manage.belongsTo(db.staff, { foreignKey: "UuidStaff" });
db.manage.belongsTo(db.dish, { foreignKey: "UuidDish" });

module.exports = db;