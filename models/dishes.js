module.exports = (sequelize, Sequelize)=>{
    const Dishes = sequelize.define('dish', {
        UuidDish: {
            type: Sequelize.UUID,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        Name:{
            type: Sequelize.STRING,
           // allowNull: false
        },
        UrlImage:{
            type: Sequelize.STRING,
        },
        Unit:{
            type: Sequelize.INTEGER
        },
        Price:{
            type: Sequelize.FLOAT
        }

    });
    return Dishes;
};