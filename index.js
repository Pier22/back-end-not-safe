const express = require("express");
const fs = require("fs");
var cors = require("cors");
const app = express();
app.use(cors());
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const config = require("./config");
const { tableControl } = require("./controllers/occupy_function");

const clientsRoutes = require("./routes/client_routes");
const staffRoutes = require("./routes/staff_routes");
const adminRoutes = require("./routes/admin_routes");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Extended: https://swagger.io/specification/#infoObject

/**@summary Il nostro Progetto e' intitolato UniBookBar e prevede la realizzazione di una piattaforma online per la gestione del bar 
 * Universitario, l'obbiettivo principale del nostro applicativo online è quello di diminuire le code e l'affollamento nelle ore di 
 * punta davanti al bar dell'università. **/

const swaggerOptions = {

    swaggerDefinition: {

        info: {

            version: "1.0.0",

            title: "API",

            description: "GENERAL",

            contact: {

                name: "Amazing Developer"

            },

            servers: ["http://localhost:3000"]

        }

    },

    // ['.routes/*.js']

    apis: ["./routes/*"]

};


const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, PATCH, DELETE"
    );
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
    next();
});

setInterval(tableControl, 86400000);

app.use("/clients", clientsRoutes);
app.use("/staff", staffRoutes);
app.use("/admin", adminRoutes);



app.listen(3000, () => console.log("listening on 3000"));

module.exports = app;