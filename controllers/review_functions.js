const db = require('../models/connection');
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' REVIEW **/


async function writeReview(req, res, client) {
    const { uuid_dish, text, title, star } = req.body;
    if (!req.body || !uuid_dish || !text || !title || !star) {
        return res.status(400).json({
            message: "contenuto vuoto",
        });
    }
    if (star > 5 || star < 0) {
        return res.status(400).json({
            message: "Il valore delle stelle non è valido",
        });
    }
    const UuidReview = uuidv4();
    return await db.review.create({
        UuidRev: UuidReview,
        Text: text,
        Title: title,
        Star: star
    }).then(() => {
        return db.writing.create({
            UuidRev: UuidReview,
            UuidClient: client,
            Date: Date.now(),
            Hour: getTime()
        })
    }).then(() => {
        return db.references.create({
            UuidRev: UuidReview,
            UuidDish: uuid_dish
        }).then(() => res.status(200).json({
            message: "operazione effettuata",
            code: 200
        }))
    })
}



async function showMyReviews(req, res, client) {
    return db.writing.findAll({
        where: {
            UuidClient: client
        },
        include: {
            model: db.review,
            attributes: ['Title', 'Text', 'star']
        }
    }).then((review) => {
        res.status(200).json(review)
    });
}



async function deleteReview(req, res) {
    const { uuid_rev } = req.body;
    if (!uuid_rev) {
        return res.status(400).json({
            message: "Il contenuto non puo essere vuoto",
        });
    }
    const review = await db.review.findOne({
        where: { UuidRev: uuid_rev }
    });
    if (!review) {
        return res.status(400).json({
            message: "La recensione non esiste",
        });
    }
    review.destroy().then(() => {
        res.status(200).json({
            message: "operazione effettuata",
            code: 200
        });
    });
}


function getTime() {
    const d = Date(Date.now());
    const time = d.split(" ")
    return time[4];
}

module.exports = {
    writeReview,
    showMyReviews,
    deleteReview
}