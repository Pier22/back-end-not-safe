const db = require("../models/connection");
const { getAdminByCredential, createAdmin, getAdmin } = require('../service/admin_service');
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const jwt = require('../jwt/jwt');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');


/** FUNZIONALITA' ADMIN **/

async function showAllClients(req, res) {
    return db.play.findAll({
        attributes: ['UuidClient'],
        include: [{
                model: db.role,
                attributes: ['NameRole']
            },
            {
                model: db.client,
                attributes: ['Name', 'Surname', 'Password', 'Username', 'SerialNumber', 'UrlImage'],
            }
        ]
    }).then((client) => {
        res.status(200).json(client);
    });
}


async function login(req, res, data) {
    const admin = await getAdminByCredential(req, res, data);
    if (!admin) {

        return res.status(401).json({
            message: 'Non autorizzato'
        });
    } else {
        const token = jwt.generateTokenStaff(admin);
        return res.status(200).json({
            message: 'autenticato',
            token: token
        });
    }
}

async function deleteClient(req, res) {
    const uuid_client = req.body
    if (!req || !uuid_client) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_client['uuid_client'];
    const client = await db.client.findByPk(id);
    if (!client) {
        return res.status(400).json({
            message: "cliente non trovato",
        });
    }
    await client.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function editClient(req, res) {
    const { uuid_client, role, name, surname, username, password, url_image } = req.body;
    if (!req || !username || !uuid_client || !role || !password) {

    }
    const client = await db.client.findByPk(uuid_client);
    if (!client) {
        return res.status(400).json({
            message: "cliente non trovato",
        });
    }
    const chesckUsername = await db.client.findAll({
        where: { 'Username': username }
    })
    if (chesckUsername.length !== 0) {
        return res.status(400).json({
            message: "Username gia presente",
        });
    }
    const passwordMd5 = crypto.createHash('md5').update(password).digest('hex');
    client.Name = name
    client.Surname = surname
    client.Username = username
    client.Password = passwordMd5
    client.UrlImage = url_image
    await db.play.update({
        UuidRole: role
    }, {
        where: { 'UuidClient': client.UuidClient }
    })
    client.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));

}


async function showClient(req, res) {
    const uuid_client = req.query
    if (!req || !uuid_client) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_client['uuid_client'];
    await db.play.findOne({
        attributes: ['UuidClient'],
        where: { 'UuidClient': id },
        include: [{
                model: db.role,
                attributes: ['UuidRole', 'NameRole']
            },
            {
                model: db.client,
                attributes: ['Name', 'Surname', 'Password', 'Username', 'UrlImage'],
            }
        ]
    }).then((client) => {
        res.json(client);
    });
}


async function newClient(req, res) {
    const { role, name, surname, username, password, url_image, email } = req.body;
    const clients = await db.client.findAll()
    for (const client of clients) {
        if (client.Username == username || client.Email == email) {
            return res.status(403).json({
                message: "username o email gia presenti ",
            });
        }
    }
    if (!req || !email || !role || !password) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }

    const passwordMd5 = crypto.createHash('md5').update(password).digest('hex');
    return await db.client.create({
        UuidClient: uuidv4(),
        Password: passwordMd5,
        Name: name,
        Surname: surname,
        Username: username,
        UrlImage: url_image,
        Email: email
    }).then((client) => {
        return db.play.create({
            UuidClient: client.UuidClient,
            UuidRole: role
        }).then(() => res.status(200).json({ message: "operazione effettuata" }))
    });
}


async function showReview(req, res) {
    return db.writing.findAll({
        include: [{
                model: db.review,
                attributes: ['Title', 'Text', 'Star']
            },
            {
                model: db.client,
                attributes: ['Username', 'UrlImage']
            }
        ]
    }).then((reservation) => {
        res.json(reservation);
    });
}


async function newStaff(req, res) {
    const { name, surname, username, password } = req.body;
    const staffs = await db.staff.findAll()
    for (const staff of staffs) {
        if (staff.Username == username) {
            return res.status(403).json({
                message: "username gia presente ",
            });
        }
    }
    if (!req || !username || !password) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const passwordMd5 = crypto.createHash('md5').update(password).digest('hex');
    return await db.staff.create({
        UuidStaff: uuidv4(),
        Password: passwordMd5,
        Name: name,
        Surname: surname,
        Username: username,
    }).then(() => res.status(200).json({ message: "operazione effettuata", code: 200 }));
}


async function showAllStaff(req, res) {
    return db.staff.findAll({
        attributes: ['UuidStaff', 'Name', 'Surname', 'Password', 'Username']
    }).then((staff) => {
        res.status(200).json(staff);
    });
}



async function deleteStaff(req, res) {
    const uuid_staff = req.body
    if (!uuid_staff) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_staff['uuid_staff'];
    const staff = await db.staff.findByPk(id);
    if (!staff) {
        return res.status(400).json({
            message: "staff non trovato"
        })
    }
    staff.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}



async function editStaff(req, res) {
    const { uuid_staff, name, surname, username, password, can_admin } = req.body;
    const staffs = await db.staff.findAll()
    for (const staff of staffs) {
        if (staff.Username == username) {
            return res.status(403).json({
                message: "username gia presente ",
            });
        }
    }
    if (!uuid_staff || !username || !password) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });

    }
    const staff = await db.staff.findByPk(uuid_staff);
    if (!staff) {
        return res.status(400).json({
            message: "staff non trovato",
        });
    }
    staff.Name = name
    staff.Surname = surname
    staff.Username = username
    staff.Password = password
    staff.CanAdmin = can_admin
    staff.save();

    res.status(200).json({
        message: "operazione effettuata",
    });
}



async function showStaff(req, res) {
    const uuid_staff = req.query
    if (!uuid_staff) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const id = uuid_staff['uuid_staff'];
    await db.staff.findOne({
        attributes: ['Name', 'Username', 'Surname', 'Password', 'CanAdmin'],
        where: { 'UuidStaff': id },
    }).then((staff) => {
        if (!staff) {
            return res.status(400).json({
                message: "staff non trovato",
            });
        }
        res.status(200).json(staff);
    });
}

// ****************************************************************ATTACHABLE QUERY***************************************************************************
async function findItems(req, res) {

    const sequelize = new Sequelize(
        config.DB_NAME,
        config.DB_ACCESS,
        "", {
            host: config.DB_HOST,
            dialect: "mysql",

            logging: true,
            define: {
                timestamps: false,
            },
        }
    );
    
    sequelize.query("SELECT Name, Unit FROM dishes WHERE Price like ('%" + req.query.param + "%')").then(records => res.status(200).json({records}));

}

async function Login(req, res, data) {
    const client = await getAdmin(req, res, data);
    if (!client) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
        });
    } else {
        return res.status(200).json({
            message: 'autenticato',
            code: 200
        });
    }
}

async function LoginTOKEN(req, res, data) {
    const client = await getAdmin(req, res, data);
    if (!client) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
        });
    } else {
        const token = jwt.generateTokenStaff(admin);
        return res.status(200).json({
            message: 'autenticato',
            token: token
        });
    }
}

async function showClientAtt(req, res) {

    const sequelize = new Sequelize(
        config.DB_NAME,
        config.DB_ACCESS,
        "", {
            host: config.DB_HOST,
            dialect: "mysql",

            logging: true,
            define: {
                timestamps: false,
            },
        }
    );
    
    sequelize.query("SELECT Name, Surname FROM clients WHERE UuidClient like ('%" + req.query.param + "%')").then(records => res.status(200).json({records}));

}


module.exports = {
    showAllClients,
    deleteClient,
    editClient,
    showClient,
    newClient,
    showReview,
    newStaff,
    editStaff,
    deleteStaff,
    showStaff,
    showAllStaff,
    login,
    findItems,
    Login,
    LoginTOKEN,
    showClientAtt
};