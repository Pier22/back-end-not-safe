const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** CHIAMATE A CATEGORY **/


async function newCategory(req, res) {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    db.category.create({
        UuidCategory: uuidv4(),
        CategoryName: name
    }).then(res.sendStatus(200));
}


async function deleteCategory(req, res) {
    const { uuid_category } = req.body;
    if (!uuid_category) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const category = db.category.findByPk(uuid_category);
    category.destroy().then(res.sendStatus(200));
}

async function renameCategory(req, res) {
    const { uudi_category, name } = req.body;
    const category = db.category.findByPk(uudi_category);
    category.Name = name;
    category.save().then(res.sendStatus(200));
}


async function getAllCategories(req, res) { //cambiare nome
    const category = db.category.findAll({
        attributes: ['CategoryName', 'UuidCategory']
    }).then((category) => res.status(200).json(category));
}

module.exports = {
    getAllCategories,
    newCategory
}