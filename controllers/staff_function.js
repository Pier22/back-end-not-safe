const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');
const jwt = require('../jwt/jwt');
const { getStaffByCredential, showStaffByUsername, createStaff } = require('../service/staff_service');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');

/** FUNZIONALITA' STAFF **/

async function registration(req, res, data) {
    const staff = await createStaff(req, res, data);
    if (staff) {

        const token = jwt.generateTokenStaff(staff);
        console.log(token);
        return res.status(200).json({
            message: 'registrato',
            token: token
        });
    } else {
        res.sendStatus(400);
    }
}

async function login(req, res, data) {
    const staff = await getStaffByCredential(req, res, data);
    if (!staff) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
            code: 401
        });
    } else {

        const token = jwt.generateTokenStaff(staff)
        return res.status(200).json({
            message: 'staff autenticato',
            token: token,
            code: 200
        });

    }
}


async function showTables(req, res) {
    const occupy = await db.occupy.findAll({
        attributes: ['UuidClient'],
        include: {
            model: db.table,
            attributes: ['UuidTable', 'NumberTable', 'Chairs', 'Occupied']
        }
    })
    const table = await db.table.findAll({
        where: { 'Occupied': false },
        attributes: ['UuidTable', 'NumberTable', 'Chairs', 'Occupied']
    });
    const result = { table, occupy }
    res.json(result)
}

async function editStaffProfile(req, res, staff) {
    const { username } = req.body;
    const staffs = await db.staff.findAll()
    for (const staff of staffs) {
        if (staff.Username == username) {
            return res.status(403).json({
                message: "username gia presente ",
            });
        }
    }
    if (!username) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const isStaff = await db.staff.findByPk(staff);
    isStaff.Username = username
    isStaff.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function showProfile(req, res, staff) {
    await db.staff.findOne({
        attributes: ['Name', 'Username', 'Surname'],
        where: { 'UuidStaff': staff },
    }).then((staff) => {
        res.json(staff);
    });
}

async function tableMap(req, res) {
    const uuid_table = req.query;
    if (!uuid_table) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const id = uuid_table['uuid_table'];
    const table = await db.table.findOne({
        where: { 'UuidTable': id },
        attributes: ['UuidTable', 'NumberTable', 'Chairs', 'Occupied']
    });
    if (!table) {
        return res.status(400).json({
            message: "tavolo non trovato",
        });
    }
    res.json(table)
}

async function editTable(req, res, staff) {
    const { uuid_table, chairs, state } = req.body
    if (!uuid_table || state.isEmpty()) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const table = await db.table.findByPk(uuid_table)
    if (!table) {
        return res.status(400).json({
            message: "tavolo non trovato",
        });
    }
    if (state == false && table.Occupied !== state) {
        await freeTableStatus(table)
    }
    table.Chairs = chairs
    table.save().then((table) => {
        db.organizes.create({
            UuidOrganize: uuidv4(),
            UuidStaff: staff,
            UuidTable: table.UuidTable
        })
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}

async function freeTableStatus(table) {
    table.Occupied = false;
    const occupy = await db.occupy.findOne({
        where: { 'UuidTable': table.UuidTable }
    })
    await occupy.destroy()
    await table.save()
}



async function freeTable(req, res) {
    const { uuid_table } = req.query;
    if (!uuid_table) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const table = await db.table.findByPk(uuid_table);
    if (!table) {
        return res.status(400).json({
            message: "il tavolo non esiste",
        });
    }
    if (table.Occupied == false) {
        return res.status(400).json({
            message: "il tavolo è gia libero"
        })
    }
    table.Occupied = false;
    await table.save().then(() => res.sendStatus(200));
}

async function categoryDish(req, res) {
    const { uuid_dish } = req.query;
    if (!uuid_dish) {
        return res.sendStatus(400);
    }
    db.belong.findAll({
        where: { 'UuidDish': uuid_dish },
        attributes: ['UuidCategory'],
        include: {
            model: db.category,
            attributes: ['CategoryName']
        }
    }).then((dish) => res.status(200).json(dish));
}

async function createTable(req, res, staff) {

    await db.table.create({
        UuidTable: uuidv4(),
        Occupied: 0,
        Chairs: 4,

    }).then(() => res.status(200).json({ message: "operazione effettuata" }));
}

module.exports = {
    registration,
    login,
    editStaffProfile,
    showTables,
    showProfile,
    tableMap,
    editTable,
    freeTable,
    categoryDish,
    createTable

}