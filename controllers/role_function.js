const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' ROLE **/


async function newRole(req, res) {
    const { name } = req.body;
    db.role.create({
        UuidRole: uuidv4(),
        NameRole: name
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function deleteRole(req, res) {
    const { uuid_role } = req.body;
    const role = await db.ingredient.findByPk(uuid_ingredient);
    role.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function renameRole(req, res) {
    const { uuid_role, name } = req.body;
    const role = await db.ingredient.findByPk(uuid_ingredient);
    role.Name = name;
    role.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function getAllRole(req, res) {
    const role = await db.role.findAll({
        attributes: ['NameRole', 'UuidRole']
    }).then((role) => res.status(200).json({ role }));
}

module.exports = {
    getAllRole,
    newRole
}