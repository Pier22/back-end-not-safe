const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');
/** FUNZIONALITA' PIATTI**/


async function createDish(req, res, staff) {
    const { name, price, category, url_image, bkble_conble, ingredients, allergens } = req.body;
    if (!req || !name || bkble_conble.isEmpty || !price || !url_image || !category) {

        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    await db.dish.create({
        UuidDish: uuidv4(),
        Name: name,
        Price: price,
        BkbleConble: bkble_conble,
        UrlImage: url_image
    }).then((dish) => {
        for (const ingredient of ingredients) {
            db.made.create({
                UuidDish: dish.UuidDish,
                UuidIngredient: ingredient
            });
        }
        return dish;
    }).then((dish) => {
        for (const allergen of allergens) {
            db.content.create({
                UuidDish: dish.UuidDish,
                UuidAllergen: allergen
            });
        }
        return dish;
    }).then((dish) => {
        db.belong.create({
            UuidDish: dish.UuidDish,
            UuidCategory: category
        })
        return dish;
    }).then((dish) => {
        console.log(staff);
        db.manage.create({
            UuidManage: uuidv4(),
            UuidDish: dish.UuidDish,
            UuidStaff: staff,
            Azione: "creazione Piatto"
        })
    }).then(() => res.status(200).json({ message: "operazione effettuata" }));
}


async function showAllDishes(req, res) {
    db.dish.findAll({
        attributes: ['UuidDish', 'Name', 'Price', 'BkbleConble', 'UrlImage']
    }).then((dish) => res.status(200).json(dish));
}



async function getAllDishes(req, res) {
    db.dish.findAll({
        attributes: ['UuidDish', 'Name', 'Price', 'BkbleConble', 'UrlImage', 'Unit']
    }).then((dish) => res.status(200).json(dish));
}


async function editDish(req, res, staff) {
    const { uuid_dish, name, price, category, url_image, bkble_conble, ingredients, allergens, quantity } = req.body;
    if (!req || !uuid_dish || !name || !price || !category || !url_image || bkble_conble.isEmpty) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findByPk(uuid_dish);
    if (!dish) {
        return res.status(400).json({
            message: "piatto non trovato",
        });
    }
    dish.Name = name;
    dish.Price = price;
    dish.UrlImage = url_image;
    dish.BkbleConble = bkble_conble;
    dish.Unit = quantity;
    dish.save();
    await db.belong.findOne({
        where: { 'UuidDish': uuid_dish }
    }).then((cat) => {
        cat.UuidCategory = category;
        cat.save();
    });
    const tmp = await db.made.findAll({
        where: { 'UuidDish': uuid_dish }
    })
    for (const temp of tmp) {
        await temp.destroy();
    }
    for (const ingredient of ingredients) {
        await db.made.create({
            UuidDish: uuid_dish,
            UuidIngredient: ingredient
        });
    }
    const tmpAllergen = await db.content.findAll({
        where: { 'UuidDish': uuid_dish }
    });
    for (const temp of tmpAllergen) {
        await temp.destroy();
    }
    for (const allergen of allergens) {
        await db.content.create({
            UuidDish: uuid_dish,
            UuidAllergen: allergen
        });
    }
    db.manage.create({
        UuidManage: uuidv4(),
        UuidDish: dish.UuidDish,
        UuidStaff: staff,
        Azione: "modifica Piatto"
    })
    res.status(200).json({
        message: "operazione effettuata",
    });
}


async function dishesByCategory(req, res) {
    const { category } = req.query;
    if (!category) {
        return res.status(400).json({

            message: "Il contenuto non può essere vuoto",
        });
    }
    const isPresent = await db.category.findOne({
        where: { UuidCategory: category }
    })
    if (!isPresent) {
        return res.status(403).json({
            message: "Categoria non trovata",
        });
    }
    await db.belong.findAll({
        where: { UuidCategory: category },
        include: {
            model: db.dish,
            attributes: ['Name', 'Price', 'UrlImage']
        }
    }).then((dish) => {
        res.status(200).json(dish)
    })
}


async function showDetailDish(req, res) {
    const { uuid_dish } = req.query;
    if (!uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findOne({
        where: { UuidDish: uuid_dish },
        attributes: ['Name', 'Urlimage']
    })
    if (!dish) {
        return res.status(400).json({
            message: "Il piatto non è stato trovato",
        });
    }
    const ingredient = await db.made.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.ingredient,
            attributes: ['Name']
        }
    });
    const allergens = await db.content.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.allergen,
            attributes: ['Name']
        }
    });
    const mediaRecensioni = await starAvg(uuid_dish);
    const json = { dish, ingredient, allergens, mediaRecensioni }
    res.status(200).json(json);
}


async function starAvg(uuid_dish) {
    const review = await db.references.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.review,
            attributes: ['Star']
        }
    });
    var tot = 0;
    var i = 0;
    for (const rev of review) {
        tot = rev.review.Star + tot;
        i = i + 1;
    }
    return [tot / i, i];
}


async function deleteDish(req, res) {
    const { uuid_dish } = req.body;
    if (!uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findByPk(uuid_dish)
    if (!dish) {
        return res.status(404).json({
            message: "piatto non trovato",
        });
    }
    const reference = await db.references.findAll({
        where: { UuidDish: uuid_dish },
        attributes: ['UuidRev']
    })
    for (const ref of reference) {
        const rev = await db.review.findByPk(ref.UuidRev)
        await rev.destroy();
        await ref.destroy();
    }
    await dish.destroy().then(() => res.status(200).json({
        message: "operazione effettuata",
    }));
}


async function showDish(req, res) {
    const { uuid_dish } = req.query;
    if (!uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const dish = await db.dish.findOne({
        where: { UuidDish: uuid_dish },
        attributes: ['Name', 'Urlimage', 'BkbleConble', 'Price']
    })
    if (!dish) {
        return res.status(400).json({
            message: "il piatto non è stato trovato",
        });
    }
    const ingredient = await db.made.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.ingredient,
            attributes: ['UuidIngredient', 'Name']
        }
    });
    const allergens = await db.content.findAll({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.allergen,
            attributes: ['UuidAllergen', 'Name']
        }
    });
    const category = await db.belong.findOne({
        where: { UuidDish: uuid_dish },
        include: {
            model: db.category,
            attributes: ['UuidCategory', 'CategoryName']
        }
    })
    var Ingredients = [];
    var Allergens = [];
    var i = 0;
    var j = 0;
    for (const ing of ingredient) {
        Ingredients[i] = ingredient[i]['Ingredient']
        i = i + 1;
    }
    for (const aller of allergens) {
        Allergens[j] = allergens[j]['Allergen']
        j = j + 1;
    }

    let Dish = {
        nameDish: dish['Name'],
        BkbleConble: dish['BkbleConble'],
        Price: dish['Price'],
        Urlimage: dish['Urlimage'],
        ingredient: Ingredients,
        allergens: Allergens,
        category: category['Category']
    }

    const json = { Dish }

    res.status(200).json(json);
}

/// QUERY ATTACCABILE
async function getDish(req, res) {
    const sequelize = new Sequelize(
        config.DB_NAME,
        config.DB_ACCESS,
        "", {
            host: config.DB_HOST,
            dialect: "mysql",

            logging: true,
            define: {
                timestamps: false,
            },
        }
    );
    
    sequelize.query("SELECT Name, Unit FROM dishes WHERE Price like ('%" + req.query.param + "%')").then(records => res.status(200).json({records}));
}


module.exports = {
    createDish,
    showAllDishes,
    editDish,
    dishesByCategory,
    showDetailDish,
    deleteDish,
    showDish,
    getDish,
    getAllDishes
}