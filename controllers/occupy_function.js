const db = require('../models/connection');
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' OCCUPY **/


async function occupyTable(req, res, client) {
    const { uuid_table } = req.body;
    if (!uuid_table) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const table = await db.table.findByPk(uuid_table)

    if (!table) {
        return res.status(404).json({
            message: "Tavolo non trovato",
        });
    }
    if (table.Occupied == true) {
        return res.status(403).json({
            message: "Tavolo gia occupato",
        });
    }
    await db.occupy.findOne({
        where: { UuidClient: client, Date: Date.now() },
        attributes: ['UuidClient']
    }).then((resu) => {
        if (resu) {
            return res.status(403).json({
                message: "Hai già effettuato una prenotazione",
            });
        } else {
            return db.occupy.create({
                UuidOccupy: uuidv4(),
                UuidClient: client,
                UuidTable: uuid_table,
                Date: Date.now(),
                Hour: getTime()
            }).then(() => {
                table.Occupied = true;
                table.save().then(res.status(200).json({
                    message: "operazione effettuata con successo",
                    code: 200
                }));
            })
        }
    });
}



async function deleteOccupation(req, res) {
    date = new Date();
    const { uuid_occupy } = req.body;
    if (!uuid_occupy) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const occupy = await db.occupy.findByPk(uuid_occupy)
    if (!occupy) {
        return res.status(400).json({
            message: "Prenotazione non trovata",
        });
    } else if (occupy.Date !== date.toISOString().split("T")[0]) {
        return res.status(400).json({
            message: "Non puoi eliminare questa prenotazione",
        });
    }
    const table = await db.table.findByPk(occupy.UuidTable);
    table.Occupied = false
    table.save();
    occupy.destroy().then(() => {
        res.status(200).json({
            message: "operazione effettuata",
            code: 200
        })
    });
}


function showMyTables(req, res, client) {
    db.occupy.findAll({
        attributes: ['Date', 'Hour', 'UuidOccupy'],
        where: { UuidClient: client },
        include: {
            model: db.table,
            attributes: ['NumberTable', 'Chairs']
        }
    }).then(table => res.status(200).json(table));
}


function showFreeTables(req, res) {
    db.table.findAll({
        where: { Occupied: false },
    }).then(freeTable => res.status(200).json(freeTable));
}


function tableControl(req, res, next) {
    setTimeout(switchOccupy, 5000)
}


function switchOccupy() {
    db.table.update({
        Occupied: false
    }, {
        where: { Occupied: true }
    })
}


function getTime() {
    const d = Date(Date.now());
    const time = d.split(" ")
    return time[4];
}

module.exports = {
    occupyTable,
    showMyTables,
    deleteOccupation,
    showFreeTables,
    tableControl
};