const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** FUNZIONALITA' INGREDIENTI **/


async function newIngredient(req, res) {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }

    const ingredientToFind = await db.ingredient.findAll({
        where: { Name: name }
    });

    if (ingredientToFind.length > 0) {
        console.log("wait")
        return res.status(403).json({

            message: "ingrediente gia' presente",
        });
    }

    db.ingredient.create({
        UuidIngredient: uuidv4(),
        Name: name
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}

async function deleteIngredient(req, res) {
    const { uuid_ingredient } = req.body;
    if (!uuid_ingredient) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const ingredient = await db.ingredient.findByPk(uuid_ingredient);
    ingredient.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}



async function renameIngredient(req, res) {
    const { uuid_ingredient, name } = req.body;
    if (!uuid_ingredient || !name) {
        return res.status(400).json({

            message: "il contenuto non puo essere vuoto",
        });
    }

    const ingredientToFind = await db.ingredient.findAll({
        where: { Name: name }
    });

    if (ingredientToFind.length > 0) {
        return res.status(404).json({

            message: "ingrediente gia' presente",
        });
    }
    const ingredient = await db.ingredient.findByPk(uuid_ingredient);
    ingredient.Name = name;
    ingredient.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function getAllIngredients(req, res) {
    const ingredient = await db.ingredient.findAll({
        attributes: ['Name', 'UuidIngredient']
    }).then((ingredients) => res.status(200).json(ingredients));
}

module.exports = {
    getAllIngredients,
    newIngredient,
    renameIngredient,
    deleteIngredient
}