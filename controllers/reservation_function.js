const db = require('../models/connection');
const { v4: uuidv4 } = require('uuid');
const { Op } = require("sequelize");

/** FUNZIONALITA' RESERVATION **/


async function reservationDish(req, res, client) {
    const { uuid_dish, quantity } = req.body;
    if (!req || !uuid_dish || !quantity || uuid_dish.length == 0 || quantity.length == 0) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }
    const UuidRes = uuidv4();
    var i = 0;
    for (const uuid of uuid_dish) {
        const dish = await db.dish.findByPk(uuid);

        if (!dish) {
            i = i + 1;
        } else {

            if ((dish.Unit - quantity[i]) < 0) {

                return res.status(400).json({
                    message: "quantità superata",
                });
            }

            if (quantity[i] <= 0) {
                i = i + 1;
            } else {

                dish.Unit = dish.Unit - quantity[i];
                await dish.save();

                await db.reservation.create({
                    UuidReservation: UuidRes,
                    UuidClient: client,
                    UuidDish: dish.UuidDish,
                    Quantity: quantity[i],
                    Date: Date.now(),
                    Hour: getTime()
                });
                i = i + 1;
            }
        }
    }
    res.status(200).json({
        message: "operazione effettuata",
        code: 200
    })
}


async function editReservation(req, res, client) {
    const { uuid_reserv, uuid_dish, quantity } = req.body;
    if (!req.body || !uuid_reserv || !uuid_dish || !quantity) {
        return res.status(400).json({
            message: "Il contenuto non puo essere vuoto",
        });
    }
    const dish = await db.dish.findByPk(uuid_dish);

    const reserv = await db.reservation.findOne({
        where: { UuidClient: client, UuidReservation: uuid_reserv, UuidDish: uuid_dish }
    });

    if (!dish || !reserv) {
        return res.status(400).json({
            message: "Campi non trovati",
        });
    }

    if (quantity <= 0) {
        return res.status(400).json({
            message: "Quantita non valida",
        });
    }

    if (reserv.Quantity < quantity) {
        dish.Unit = dish.Unit - (quantity - reserv.Quantity);
        if (dish.Unit < 0) {
            return res.status(422).json({
                message: "Errore, quantità superata",
            });
        }
        await dish.save();
    } else {

        dish.Unit = dish.Unit + (reserv.Quantity - quantity);
        await dish.save();
    }
    reserv.Quantity = quantity;
    await db.reservation.update({ Quantity: quantity }, {
        where: { UuidReservation: uuid_reserv, UuidClient: client, UuidDish: uuid_dish }
    }).then(() => res.status(200).json({
        message: "operazione effettuata",
        code: 200
    }));

}


async function showMyReservations(req, res, client) {
    var date = new Date();

    const pren = await db.reservation.findAll({
        attributes: ['UuidReservation', 'Quantity'],
        include: {
            model: db.dish,
            attributes: ['Name']
        },
        where: {
            Date: {
                [Op.eq]: date.toISOString().split("T")[0]
            },
            UuidClient: client
        },
        group: ['UuidReservation'],
    })


    let arrayClass = []
    let i = 0

    for (const resr of pren) {

        arrayClass[i] = await calculateTotReservation(req, res, resr.UuidReservation);
        i = i + 1;
    }
    res.json(arrayClass);


}


async function calculateTotReservation(req, res, idReserv) {
    const dishesReserved = await db.reservation.findAll({
        attributes: ['UuidDish', 'Quantity'],
        where: { 'UuidReservation': idReserv }
    });
    let total = 0;
    let quantity = [];
    let arraydish = [];
    let arrayPrice = [];
    let array_uuid_dish = [];
    let i = 0;
    for (const reserved of dishesReserved) {
        quantity[i] = reserved.Quantity;
        const dish = await db.dish.findByPk(reserved.UuidDish);
        arraydish[i] = dish.Name;
        arrayPrice[i] = dish.Price;
        array_uuid_dish[i] = dish.UuidDish;
        total = (dish.Price * reserved.Quantity) + total;
        i = i + 1
    }

    let tempClass = {
        uuidReserv: idReserv,
        total1: total,
        quantity1: quantity,
        dish: arraydish,
        price: arrayPrice,
        uuid_dish: array_uuid_dish
    };
    return (tempClass);
}



function showAllReservations(req, res, client) {
    return db.reservation.findAll({
        attributes: ['UuidReservation', 'UuidDish'],
        where: { UuidClient: client },
        include: [{
            model: db.dish,
            attributes: ['Name', 'UrlImage']
        }]
    }).then(reservation => res.status(200).json(reservation));
}



async function deleteDishesfromReservation(req, res, client) {
    const { uuid_dish, uuid_reserv } = req.body;
    if (!req.body || !uuid_reserv || !uuid_dish) {
        return res.status(400).json({
            message: "Il contenuto non puo essere vuoto",
        });
    }
    for (const uuid of uuid_dish) {
        const dish = await db.dish.findByPk(uuid);
        const reserv = await db.reservation.findOne({
            where: { UuidClient: client, UuidReservation: uuid_reserv, UuidDish: uuid }
        });
        if (!dish || !reserv) {
            return res.status(400).json({
                message: "Campi non trovati",
            });
        }
        dish.Unit = dish.Unit + reserv.Quantity;
        await dish.save();
        db.reservation.destroy({
            where: {
                UuidReservation: uuid_reserv,
                UuidDish: uuid,
                UuidClient: client
            }
        });
    }
    res.status(200).json({
        message: "operazione effettuata",
        code: 200
    });
}


function validateReservation(req, res, next) {
    const hour = getTime().split(":")
        //PER FAVORIRE I TEST ABBIAMO IMPOSTATO IL FILTRO SULL'ORA 'hour[0] >= 11 && hour[0] <= 18' >=0
    if (hour[0] >= 0) {
        next()
    } else {
        res.status(403).json({
            message: "Le prenotazioni si possono effetuare dalle 11:00 alle 18:00",
        })
    }
}


function getTime() {
    const d = Date(Date.now());
    const time = d.split(" ")
    return time[4];
}



async function deleteReservation(req, res, client) {
    const { uuid_reserv } = req.body;
    if (!uuid_reserv) {
        return res.status(400).json({
            message: "Il contenuto non puo essere vuoto",
        });
    }
    const dish_reserve = await db.reservation.findAll({
        where: { UuidClient: client, UuidReservation: uuid_reserv },
        attributes: ['UuidDish', 'Quantity', 'UuidReservation'],
    });


    for (const dish of dish_reserve) {
        const dish_searched = await db.dish.findByPk(dish['UuidDish']);

        dish_searched.Unit = dish_searched.Unit + dish.Quantity;
        await dish_searched.save();
        dish.destroy();

    }
    res.status(200).json({
        message: "Cancellazione effettuata",
        code: 200
    });
}

module.exports = {
    reservationDish,
    showAllReservations,
    editReservation,
    validateReservation,
    deleteDishesfromReservation,
    showMyReservations,
    deleteReservation
};