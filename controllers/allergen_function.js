const db = require("../models/connection");
const { v4: uuidv4 } = require('uuid');

/** CHIAMATE AD ALLERGENI **/


async function newAllergen(req, res) {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }

    const allergenToFind = await db.allergen.findAll({
        where: { Name: name }
    });

    if (allergenToFind.length > 0) {
        return res.status(403).json({

            message: "allergene gia' presente",
        });
    }
    await db.allergen.create({
        UuidAllergen: uuidv4(),
        Name: name
    }).then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function deleteAllergen(req, res) {
    const { uuid_allergen } = req.body;
    if (!uuid_allergen) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }
    const allergen = await db.allergen.findByPk(uuid_allergen);
    allergen.destroy().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}



async function renameAllergen(req, res) {
    const { uuid_allergen, name } = req.body;
    if (!uuid_allergen || !name) {
        return res.status(400).json({
            message: "il contenuto non puo essere vuoto",
        });
    }

    const allergenToFind = await db.allergen.findAll({
        where: { Name: name }
    });

    if (allergenToFind.length > 0) {
        return res.status(403).json({

            message: "allergene gia' presente",
        });
    }

    const allergen = await db.allergen.findByPk(uuid_allergen);
    allergen.Name = name;
    allergen.save().then(() => res.status(200).json({
        message: "Operazione effettuata"
    }));
}


async function getAllAllergens(req, res) {
    const allergens = db.allergen.findAll({
        attributes: ['Name', 'UuidAllergen']
    }).then((allergens) => res.status(200).json(allergens));
}

module.exports = {
    getAllAllergens,
    newAllergen,
    renameAllergen,
    deleteAllergen
}