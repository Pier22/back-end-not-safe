const jwt = require('../jwt/jwt');
const { getUserByCredential, createUser,getUser } = require('../service/client_service');
const db = require('../models/connection')
const crypto = require('crypto');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');

/** CHIAMATE ALLE FUNZIONALITA CLIENTE **/

async function registration(req, res, data) {
    const client = await createUser(req, res, data);

    if (client) {
        const token = jwt.generateTokenClient(client);
        return res.status(200).json({
            message: 'registrato',
            token: token,
            code: 200
        });
    } else {
        res.status(400).json({
            message: 'qualcosa è andato storto',
        });
    }
}


async function getAllDishesByCategory(req, res) {
    db.belong.findAll({
        attributes: ['UuidDish', 'UuidCategory'],
        include: {
            model: db.dish,
            attributes: ['Name', 'Price', 'Unit', 'UrlImage']
        }
    }).then((dish) => res.status(200).json(dish));
}


async function login(req, res, data) {
    const client = await getUserByCredential(req, res, data);
    if (!client) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
        });
    } else {
        const token = jwt.generateTokenClient(client);
        return res.status(200).json({
            message: 'autenticato',
            token: token,
            code: 200
        });
    }
}


async function showAccount(req, res, cliente) {
    return db.client.findOne({
        where: { 'UuidClient': cliente },
        attributes: ['Name', 'Surname', 'SerialNumber', 'Username', 'Password', 'UrlImage']
    }).then((user) => res.status(200).send(user));
}



async function editAccount(req, res, client) {
    const { name, surname, username, password, url_image } = req.body;

    if (!req || !username) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }

    const c = await db.client.findByPk(client);
    if (name) {
        c.Name = name;
    }

    if (surname) {
        c.Surname = surname;
    }

    if (password) {
        if (password.length < 5) {
            return res.status(403).json({
                message: "password troppo corta",
            });
        }
        c.Password = crypto.createHash('md5').update(password).digest('hex');
    }

    if (url_image) {
        c.UrlImage = url_image;
    }

    c.Username = username;

    c.save().then(() => res.status(200).json({
        message: "operazione effettuata",
        code: 200
    }))
}


async function showReview(req, res) {
    const { title } = req.query;

 
   if (!title) {
        return res.status(400).json({
            message: "Il contenuto non può essere vuoto",
        });
    }


    return db.review.findAll({
        where: { 'Title': title },
         attributes: ['Title', 'Text', 'Star']
         
    }).then((final) => {
        res.json(final);
    });
}

async function showAllStaff(req, res) {
    return db.staff.findAll({
        attributes: ['UuidStaff', 'Name', 'Surname', 'Password', 'Username']
    }).then((staff) => {
        res.status(200).json(staff);
    });
}

// *************************************************************ATTACHABLE QUERY*********************************************************

async function showReviewByTitle(req, res) {

    const sequelize = new Sequelize(
        config.DB_NAME,
        config.DB_ACCESS,
        "", {
            host: config.DB_HOST,
            dialect: "mysql",

            logging: true,
            define: {
                timestamps: false,
            },
        }
    );
    
    sequelize.query("SELECT Text FROM reviews WHERE Title like ('%" + req.query.param + "%')").then(records => res.status(200).json({records}));


}

async function Login(req, res, data) {
    const client = await getUser(req, res, data);
    if (!client) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
        });
    } else {
        return res.status(200).json({
            message: 'autenticato',
            code: 200
        });
    }
}

async function LoginTOKEN(req, res, data) {
    const client = await getUser(req, res, data);
    if (!client) {
        return res.status(401).json({
            message: 'Non autorizzato, credenziali errate',
        });
    } else {
        const token = jwt.generateTokenClient(client);
        return res.status(200).json({
            message: 'autenticato',
            token: token,
            code: 200
        });
    }
}


async function test(req, res) {

    const sequelize = new Sequelize(
        config.DB_NAME,
        config.DB_ACCESS,
        "", {
            host: config.DB_HOST,
            dialect: "mysql",

            logging: true,
            define: {
                timestamps: false,
            },
        }
    );
    
    db.client.findAll( { where: { name: req.body.name } } ).then((client) => {
        console.log(db.client.findAll( { where: { name: req.body.name } } ))
        res.status(200).json(client);
        
    });

}

async function test2(req, res) {

    const sequelize = new Sequelize(
        config.DB_NAME,
        config.DB_ACCESS,
        "", {
            host: config.DB_HOST,
            dialect: 'mariadb',
            dialectOptions: {connectTimeout: 1000},// mariadb connector option

            logging: true,
            define: {
                timestamps: false,
            },
        }
    );
    
    db.client.findAll({
        order:"' ) DELETE FROM `Users` WHERE 1=1; --",
      }).then((staff) => {
        res.status(200).json(staff);
    });

}

//'1; DELETE FROM "Users" WHERE 1=1; --'
module.exports = {
    registration,
    login,
    showAccount,
    editAccount,
    getAllDishesByCategory,
    showReviewByTitle,
    showReview,
    Login,
    test,
    test2,
    LoginTOKEN
  
};