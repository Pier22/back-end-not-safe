const { verifyToken, ensureToken } = require('../jwt/jwt');

/** MODULO DI MIDDLEWARE CONTENENTE LE FUNZIONI DI CHECK DEI PERMESSI TRAMITE TOKEN (PER OGNI ENTITA') **/

/** 
 * FIRMA E SCANDENZA DEL TOKEN  https://www.npmjs.com/package/jsonwebtoken
  jwt.sign({
  exp: Math.floor(Date.now() / 1000) + (60 * 60),
  data: 'foobar'
}, 'secret'); 

------------------------------  FIRMA CERTIFICATI
var cert = fs.readFileSync('public.pem');  // get public key
jwt.verify(token, cert, { audience: 'urn:foo', issuer: 'urn:issuer' }, function(err, decoded) {
  // if issuer mismatch, err == invalid issuer
});

-------------------------------CONTROLLO SULLA SCADENZA
jwt.sign({
  data: 'foobar'
}, 'secret', { expiresIn: '1h' });
*/

function isClient(req, res, next) {
    const isToken = ensureToken(req, res)
    //isToken.expireIn:1H
    if (isToken === null) {
        res.sendStatus(403).json({
            message: "non valido"
        });
    } else {
        const entity = verifyToken(req, res, next)
        if (entity.uuidClient) {
            return entity.uuidClient
        } else {
            res.sendStatus(401).json({
                message: "non sei un cliente"
            })
            return false;
        }

    }
}

function isStaff(req, res, next) {
    const isToken = ensureToken(req, res)
    //isToken.expireIn:1H
    if (isToken === null) {
        res.sendStatus(403);
    } else {
        const entity = verifyToken(req, res, next)
        console.log('okokokok')
        if (entity.uuidStaff && entity.canAdmin == 0) {
            return entity.uuidStaff
        } else {
            res.status(401).json({
                message: "Non sei uno staff"
            })
            return false;
        }
    }
}

function isAdmin(req, res, next) {
    const isToken = ensureToken(req, res)
    //isToken.expireIn:1H
    if (isToken === null) {
        res.sendStatus(403);
    } else {
        const entity = verifyToken(req, res, next)
        console.log(entity)
        if (entity.uuidStaff && entity.canAdmin == 1) {
            return entity.uuidStaff
        } else {
            res.status(401).json({
                message: "Non sei uno admin"
            })
            return false;
        }
    }
}

function isAdminStaff(req, res, next) {
    const isToken = ensureToken(req, res)
    //isToken.expireIn:1H
    if (isToken === null) {
        res.sendStatus(403);
    } else {
        const entity = verifyToken(req, res, next)

        if (entity.uuidStaff && entity.canAdmin == 1) {
            return entity.uuidStaff
        }
        if (entity.uuidStaff && entity.canAdmin == 0) {
            return entity.uuidStaff
        } else {
            res.status(401).json({
                message: "non autorizzato"
            })
            return false;
        }
    }
}


module.exports = {
    isClient,
    isStaff,
    isAdmin,
    isAdminStaff
};
