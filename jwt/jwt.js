const jwt = require('jsonwebtoken');
const config = require('../config')

/** MODULO CONTENENTE LE FUNZIONI DI GENERAZIONE DEL TOKEN (PER OGNI ENTITA') E LE VARIE FUNZIONI
 * DI 'VERIFY' ED 'ENSURE'
 **/


//GENERA TOKEN CLIENTE
function generateTokenClient(params) {
    return jwt.sign({
        uuidClient: params.UuidClient,
        email: params.Email,
        name: params.Name,
        surname: params.Surname,
    }, config.JWT_SECRET);
}


//GENERA TOKEN STAFF
function generateTokenStaff(params) {
    return jwt.sign({
        uuidStaff: params.UuidStaff,
        username: params.Username,
        name: params.Name,
        surname: params.Surname,
        canAdmin: params.CanAdmin,
    }, config.JWT_SECRET);
}


/**VERIFICA TOKEN
 * @param(req, res)
 * @returns(entity data)
 * @response : 403
 */
function verifyToken(req, res) {
    return jwt.verify(req.token, config.JWT_SECRET, (err, data) => {
        if (err) {
            res.status(403);
            return false
        } else {
            return (data)
        }
    })
}


//CHECK DEL  TOKEN, E RELATIVA PRESENZA NELL'HEADER
function ensureToken(req, res) {
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
    } else {
        res.status(403).json({ message: "token not in header" });
    }
}


module.exports = {
    verifyToken,
    generateTokenClient,
    generateTokenStaff,
    ensureToken
};
