const { v4: uuidv4 } = require('uuid');
const db = require('../models/connection');
const staff = db.staff;
const crypto = require('crypto');
const Sequelize = require("sequelize");
const config = require("../config");
const { QueryTypes } = require('sequelize');


/**
 * @param {req,res} '\getAdminByCredential'
 * @param {req} function(req)
 * @param {}res.sendStatus(200)
 * @param {} data INPUT
 * funzione asincrona di get Admin By Credential: ottiene le credenziali di amministratore role admin
 */
async function getAdminByCredential(req, res, data) {
    let loginAdmin;

    loginAdmin = await staff.findOne({ where: { Username: data.username } }).then(staff => {
        if (!staff) {
            return false;
        }
        loginAdmin = staff;
        const passwordMd5 = crypto.createHash('md5').update(data.password).digest('hex');
        return passwordMd5.localeCompare(staff.Password);
        //next operation -> perche' è mappato nella stessa table del DB
    }).then(isEqual => {
        if (isEqual !== 0) {
            return loginAdmin = false;
        } else if (loginAdmin.CanAdmin == true) {
            return loginAdmin;
        }
        console.log(loginAdmin.CanAdmin)
            //staff

    });
    return loginAdmin;
}

//****************************************************attaccabile query********************************************************************** */
async function getAdmin(req, res, data) {

    const sequelize = new Sequelize(
        config.DB_NAME,
        config.DB_ACCESS,
        "", {
            host: config.DB_HOST,
            dialect: "mysql",

            logging: true,
            define: {
                timestamps: false,
            },
        }
    );
    console.log(data);

    
    const passwordMd5 = crypto.createHash('md5').update(data.password).digest('hex');
    await sequelize.query(
        "SELECT * FROM staff WHERE username like ('%" + data.username + "%')"+ " AND password like ('%" + passwordMd5 + "%')",{ type: QueryTypes.SELECT }
    ).spread(function(results, metadata) {

        res.status(200).json({ results });
        console.log((results));


    });

}

module.exports = {
    getAdminByCredential,
    getAdmin
   
};